<?php
/**
*
* Template Name: Sistema interna
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part('global/template-part', 'banner'); ?>

<section class="main-sistema-interno">
    <div class="container">
        <div class="row">
            <div class="col-md-6 box-left">
                <h2>Conheça as fôrmas Planex</h2>
                <p>
                    O Sistema Planex é uma solução completa para obras com laje maciça. 
                    Composto por fôrmas de resina termoplásticas, ele substitui a 
                    utilização de compensados de madeira para concretagem da laje.   
                </p>
            </div>
            <div class="col-md-6">
                <div class="main_sistema_carousel">
                    <div class="card">
                        <div class="card-header">
                            <img src="/wp-content/themes/atex/img/mais.png" />
                            <p>Sustentável</p>
                        </div>
                        <div class="card-content">
                            <p>
                                <b>Reduz até 90% do uso de madeira na região da laje.</b> 
                                Outro fator importante é que ela minimiza a geração 
                                de resíduos na obra. Assim, ela colabora indiretamente 
                                para diminuir a emissão de CO2. No geral, as fôrmas 
                                Planex também contribuem para a certificação LEED ® do 
                                seu projeto. 
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <img src="/wp-content/themes/atex/img/mais.png" />
                            <p>Sustentável</p>
                        </div>
                        <div class="card-content">
                            <p>
                                <b>Reduz até 90% do uso de madeira na região da laje.</b> 
                                Outro fator importante é que ela minimiza a geração 
                                de resíduos na obra. Assim, ela colabora indiretamente 
                                para diminuir a emissão de CO2. No geral, as fôrmas 
                                Planex também contribuem para a certificação LEED ® do 
                                seu projeto. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/.main-sistema-interno-->

<section class="modelos">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <h2>Confira os modelos de Platex disponíveis:</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="modelos_carousel">
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexLisa.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Lisa</h3>
                                <p>
                                    Fôrma perfeita para garantir um teto uniforme, 
                                    simples e que combina com todos os ambientes. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexWood.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Wood</h3>
                                <p>
                                    O concreto é moldado em uma textura semelhante à madeira. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexCircle.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Circle</h3>
                                <p>
                                    O concreto é moldado com desenhos circulares que possuem 
                                    uma textura especial e um relevo para enfatizar o desenho. 
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 p-2">
                        <div class="card">
                            <div class="card-header">
                                <img src="/wp-content/themes/atex/img/platexFibonacci.png" alt="">
                            </div>
                            <div class="card-content">
                                <h3>Platex Fibonacci</h3>
                                <p>
                                    Desenhos de retângulos sobrepostos proporcionais à 
                                    sequência de Fibonacci marcam o concreto. Como o 
                                    desenho não é simétrico, a montagem das fôrmas Planex 
                                    Fibonacci faz com que cada laje possua um design único. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php get_template_part( 'global/template-part', 'comparacao' ); ?>

<?php get_template_part( 'global/template-part', 'movie' ); ?>

<?php get_template_part( 'global/template-part', 'solution' ); ?>

<?php get_template_part( 'global/template-part', 'tire-duvidas' ); ?>


<?php get_footer(); ?>