<?php
/**
*
* Template Name: Home
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<section class="banner_slick">
    <?php if ( have_rows( 'cadastro_de_banners' ) ) : ?>
        <?php while ( have_rows( 'cadastro_de_banners' ) ) : the_row(); ?>
            <div class="banner_home" style="background-image:url('<?php the_sub_field( 'imagem_de_fundo' ); ?>');">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-md-12 text-left">
                            <h1 class="d-block"><?php the_sub_field( 'titulo', false, false ); ?></h1>
                            <p><?php the_sub_field( 'subtitulo' ); ?></p>
                            <hr>
                            <?php $botao = get_sub_field( 'botao' ); ?>
                            <?php if ( $botao ) : ?>
                                <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>" rel="modal:open">
                                    <button class="btn btn_first"><?php echo esc_html( $botao['title'] ); ?></button>
                                </a>
                            <?php endif; ?>
                            <a href="/#p-obras"><img class="mouse-banner" src="/wp-content/themes/atex/img/svg/mouse.svg" alt=""></a>
                        </div>
                        
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php else : ?>
        <?php // no rows found ?>
    <?php endif; ?>
</section><!-- /.main -->

 <?php get_template_part( 'global/template-part', 'solution' ); ?>

<section class="obra">
    <div class="container">
        <div class="row">
            <div class="col-md-5 ">
                <h2><?php the_field( 'titulo_por_que_escolher' ); ?></h2>
            </div>

            <div class="col-md-7">
                <ul class="obra_atex">
                    <?php if ( have_rows( 'privilegios' , 'option' ) ) : ?>
                        <?php while ( have_rows( 'privilegios' , 'option' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'privilegios_icons' ) ) : ?>
                                <li class="obra_content">
                                    <img class="pb-3" src="<?php the_sub_field( 'privilegios_icons' ); ?>" />
                                    <?php endif ?>
                                    <!-- alinhar sobre essa verificação -->
                                    <p><b><?php the_sub_field( 'privilegios_texto' ); ?></b></p>
                                </li>
                        <?php endwhile; ?>
                        <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>   
                </ul>
                <?php $botao_pq = get_field( 'botao_pq' ); ?>
                <?php if ( $botao_pq ) : ?>
                    <a href="<?php echo esc_url( $botao_pq['url'] ); ?>" target="<?php echo esc_attr( $botao_pq['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-0 mx-md-auto mb-4 justify-content-center"><?php echo esc_html( $botao_pq['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.obra -->

 <?php get_template_part( 'global/template-part', 'aqui-tem-atex' ); ?>
 
 <?php get_template_part( 'global/template-part', 'avaliacoes' ); ?>

 <?php get_template_part( 'global/template-part', 'blog' ); ?>



<?php get_footer(); ?>