<!DOCTYPE html>
<?php
$current_user = wp_get_current_user();
if ( ! user_can( $current_user, "subscriber" ) ){ // Check user object has not got subscriber role
    $user_logged = 'not';
} else {
$user_logged = 'subs';
?>
<style>
  .subs #wpadminbar, .subs .menupop, .subs #wpadminbar>#wp-toolbar>#wp-admin-bar-top-secondary>#wp-admin-bar-search #adminbarsearch input.adminbar-input {
    display: none;
}
li#wp-admin-bar-my-account{
  display: block !important;
}
li#wp-admin-bar-my-account img{
  display: none;
}
html.subs{
  margin-top: 0 !important;
}
</style>
<?php }?>

<html <?php language_attributes(); ?> class="<?php echo $user_logged;?>">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>

</head>



<body <?php body_class( $user_logged); ?> id="atex">


    <nav class="navbar navbar-expand-xl navbar-default fixed-top" role="navigation" id="nav_main">
        <div class="container-fluid">
            <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' loading='lazy'>
            </a>
            <!-- Brand and toggle get grouped for better mobile display -->
            <button type="button" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>



            <?php
            wp_nav_menu(array(
                'theme_location'    => 'primary',
                'depth'             => 2,
                'container'         => 'div',
                'container_class'   => 'collapse navbar-collapse',
                'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav ml-auto pl-4',
                'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                'walker'            => new WP_Bootstrap_Navwalker(),
            ));
            ?>
            <!--<div class="nav_function">
                <ul>
                    <li id="menu-item-1997" class="search-btn menu-item menu-item-type-custom menu-item-object-custom menu-item-1997 nav-item"><a itemprop="url" href="#" class="nav-link"><span itemprop="name"><img src="http://atex.open/wp-content/uploads/2020/09/search.png" class="img-fluid"></span></a></li>
                    <li id="menu-item-1995" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1995 nav-item"><a itemprop="url" href="#" class="nav-link"><span itemprop="name"><img src="/wp-content/uploads/2020/09/signs.png" class="img-fluid"></span></a></li>
                </ul>
            </div>-->
        </div>
    </nav>

<div class="search-box search-elem">
  <button class="close">x</button>
  <div class="inner row justify-content-center">
    <div class="small-12 columns">
      <?php echo do_shortcode('[searchandfilter id="1998"]');?>
    </div>
  </div>
 </div>


 <!-- Modal -->
<div class="modal" id="orcamento" tabindex="-1" role="dialog" aria-labelledby="orcamento">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <a href="#" rel="modal:close"><i class="fas fa-times-circle"></i></a>
      </div>
      <div class="modal-body">
         <?php the_field( 'orcamento', "option" ); ?>
        <script data-b24-form="inline/9/ctg2pg" data-skip-moving="true">
          (function(w,d,u){
                  var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0);
                  var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
          })(window,document,'https://cdn.bitrix24.com.br/b11866941/crm/form/loader_9.js');
        </script>
      </div>
    </div>
  </div>
</div>
<style>
@media screen and (min-width: 992px){
	.modal#orcamento{
    background: transparent;
		max-width: 800px;
	}
	.modal-content{
		border: none;
  }
  select{
    -webkit-appearance:none;

  }
}
</style>
<!-- Modal pop-up -->
<div id="pop-up-modalz" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
          <h2>Tenha uma experiência personalizada</h2>
          <p>Para melhorar sua experiência e te ajudar a encontrar o que você procura, precisamos que aceite o uso de cookies e nossa <a href="https://www.atex.com.br/produto/politica-privacidade.html">política de privacidade.</a>  </p>
      </div>
      <div class="btn-modal">
        <div class="modal-footer">
            <a href="#close-modal" rel="modal:close">
              <button type="button" class="btn btn-default" data-dismiss="modal">Aceitar e Continuar</button>
            </a>
        </div>
      </div>
    </div>
  </div>
</div>