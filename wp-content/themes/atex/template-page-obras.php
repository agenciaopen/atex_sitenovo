<?php

/**
 *
 * Template Name: Obras
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part('global/template-part', 'banner'); ?>


<section id="content" class="template_obra">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            <div class="row col-12 mx-0 mb-4 p-5 justify-content-center align-items-center container_filter rounded">
                <div class="col-md-12 pl-md-0 pr-md-0 text-center d-none d-md-inline obras_result_advanced" id="obras_result_advanced">
                    <?php echo do_shortcode('[searchandfilter id="106"]') ?>
                </div>
                <div class="col-12 p-0 col-md-2 container_btn rounded d-none d-none">
                    <button type="button" class="btn btn_filtrar" data-toggle="modal" data-target="#advanced-search">
                        Filtrar
                    </button>
                </div>
                <div class="col-12 col-md-5 p-0 px-md-5 pt-md-5 pb-md-3 text-center">
                    <h4 class="obra_search_title py-3 p-md-0" id="title">     
                    </h4>
                </div>
                <div class="col-12 col-md-8 p-0 px-md-4 pb-md-2 text-center">
                    <p class="obra_search_text py-3 p-md-0">
                        Já são mais de 50 milhões de metros quadrados construídos pelo Brasil, América Latina e Europa, com as soluções modulares da Atex. Confira algumas obras feitas com auxílio do nosso sistema construtivo que se encaixam na sua busca.
                    </p>
                </div>
            </div>
            <div class="col-md-10 pl-md-0 text-center simple-filter d-block d-md-none obras_result_advanced">
                <?php echo do_shortcode('[searchandfilter id="106"]') ?>
                <hr class="hr_obra_banner">
            </div>
            <div class="col-md-12" id="obras_result">

                <?php echo do_shortcode('[searchandfilter id="106" show="results"]') ?>

            </div>
            <div class="col-md-12 result_advanced" id="obras_result_adv">
                    <?php echo do_shortcode('[searchandfilter id="332" show="results"]') ?>
                </div>
            <!-- implementar funcionalidade do ajax load more no botao -->
            <div class="alm-btn-wrap d-none" style="visibility: visible;">
                <button class="alm-load-more-btn more btn btn_first col-md-3 mt-4 mb-4 blog-button" rel="next">Mostrar mais</button>
            </div>
            <!-- implementar funcionalidade do ajax load more no botao -->

            
        </div>
    </div>
</section>
<!--/.content-->

<!-- Modal -->
<div class="modal fade" id="advanced-search" tabindex="-1" role="dialog" aria-labelledby="advanced-searchLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#close-modal" rel="modal:close">
                        <button type="button" data-dismiss="modal"><i class="fas fa-times-circle"></i></button>
                    </a>
                </div>
                <div class="col-md-12" id="obras_result_advanced">
                    <?php echo do_shortcode('[searchandfilter id="332"]') ?>
                </div>

            </div>
        </div>
    </div>

<?php get_footer(); ?>
<script>
    $('#obras_result_advanced .sf-field-submit input[type="submit"]').click(function () {
        $('#obras_result').hide();
        $("#close-modal").trigger({ type: "click" });

        //$('#advanced-search').modal('hide');

        var title = $("#search-filter-form-332 input[name='_sf_search[]']" ).val();
	if (title ==''){

	}
    else{
        $('html, body').animate({
        scrollTop: $('#obras_result_adv').offset().top
    }, 'slow');
		$('#title').html('Exibindo obras referentes à sua busca por <br>"' + title + '"');
	}
});
	
//$('.sf-field-submit input[type="submit"]').click(function () {
   
    var title = $("#search-filter-form-106 input[name='_sf_search[]']" ).val();
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
let termSearch = getUrlParameter('_sf_s');
let url = window.location.href;
if(url.includes('?')){
    $('html, body').animate({
        scrollTop: $('#obras_result').offset().top
    }, 'slow');
}
	console.log(termSearch);
	if (title ==''){

	}
    else{
        $('html, body').animate({
        scrollTop: $('#obras_result').offset().top
    }, 'slow');
		$('#title').html('Exibindo obras referentes à sua busca por <br>"' + title + '"');
	}
   
//});
	
</script>