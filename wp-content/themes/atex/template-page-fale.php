<?php
/**
*
* Template Name: Fale Conosco
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php get_template_part( 'global/template-part', 'banner' ); ?>

<section class="career">
    <div class="container">
        <div class="row">
            <div class="col-md-4 pb-5">
                <h2><?php the_field( 'titulo_fale_conosco' ); ?></h2>
            </div>
            <div class="col-md-8"></div>
            <div class="form-card-content col-md-4">
                <?php the_field( 'descricao_fale_conosco' ); ?>
            </div>
            <div class="form-card-header col-md-8">
                <?php the_field( 'formulario_fale_conosco' ); ?>
            </div>
        </div>
    </div>
</section>

<section class="form-fale">
    <div class="container-fluid h1-100">
        <div class="row fale-row">
            <div class="col-md-11 card">
                <div class="col-md-4 card-header">
                    <h2><?php the_field( 'titulo_unidades_de_distribuicao' ); ?></h2>
                    <?php the_field( 'descricao_unidades_de_distribuicao' ); ?>
                </div>
                <div class="col-md-6 card-content">
                    
                    <?php echo do_shortcode('[searchandfilter id="328"]')?>

                        <?php echo do_shortcode('[searchandfilter id="328" show="results"]')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
