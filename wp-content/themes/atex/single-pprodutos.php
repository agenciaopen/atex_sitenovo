<?php
/**
*
* single page for cpt planos
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php while ( have_posts() ) : the_post(); ?>
    
    <?php get_template_part( 'global/template-part', 'banner-produtos' ); ?>

    
<section class="budget">
    <div class="container">
        <div class="row">
            <div class="col-md-12 order-2 order-md-1">
                
            </div>
            <div class="col-md-4 order-1 order-md-2 card-img">
                <?php if ( get_field( 'videoimagem' ) == 1 ) : ?>
                    
                    
                    <iframe style="height: 100%;" id="product_movie" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="auto" type="text/html" src=""></iframe>
                    <input id="product_movieID" type="hidden" value="<?php echo the_field( 'video_exemplo_de_produto' ); ?>">

                    <script>
                        var video_caller = function() {
                        var get_vdId = document.getElementById('product_movieID').value;
                        var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                        var set_victim = document.getElementById('ytplayer');

                        if (get_vdId.includes('https://youtu.be/') == true) {
                            var str_t = get_vdId.replace('https://youtu.be/', '');

                            document.getElementById('product_movie').setAttribute('src', 'https://www.youtube.com/embed/' +
                                str_t + '?rel=0?enablejsapi=1&autoplay=0');

                        } else {
                            var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                            document.getElementById('product_movie').setAttribute('src', 'https://www.youtube.com/embed/' +
                                str_t + '?rel=0?enablejsapi=1&autoplay=0');
                            }
                        }

                        try {
                            video_caller();
                        } catch (error) {
                            console.log(error);
                        }
                    </script>
                    
                <?php else : ?>

                    <div class="galery_product">
                        <?php $imagem_exemplo_de_produto_images = get_field( 'imagem_exemplo_de_produto' ); ?>
                        <?php if ( $imagem_exemplo_de_produto_images ) :  ?>
                            <?php foreach ( $imagem_exemplo_de_produto_images as $imagem_exemplo_de_produto_image ): ?>
                                <a href="<?php echo esc_url( $imagem_exemplo_de_produto_image['url'] ); ?>">
                                    <img src="<?php echo esc_url( $imagem_exemplo_de_produto_image['sizes']['medium'] ); ?>" alt="<?php echo esc_attr( $imagem_exemplo_de_produto_image['alt'] ); ?>" />
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-6 order-3 card-content">
                <h2><?php the_field( 'titulo_conteudo_da_pagina' ); ?></h2>
                <p><?php the_content();?></p>
                <?php $botao = get_field( 'link_orcamento', $page_ID ); ?>
                <?php if ( $botao ) : ?>
                    <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>" rel="modal:open">
                        <button class="btn btn_first mt-4 mb-4"><?php echo esc_html( $botao['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="product-select">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="select">
                    <select>
                        <?php if ( have_rows( 'cadastro_de_variacoes_do_produto' ) ) : ?>
                            <?php $count = 1; while ( have_rows( 'cadastro_de_variacoes_do_produto' ) ) : the_row(); ?>
                               
                                <?php $imagem_tabela = get_sub_field( 'imagem_tabela' ); ?>
                                <?php if ( $imagem_tabela ) : ?>
                                    <option value="<?php echo $count; ?>" <?php if($count == 1):?> selected <?php endif; ?>> <?php the_sub_field( 'titulo' ); ?></option>
                                <?php endif; ?>
                            <?php $count++; endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                    </select>
                    <div class="select_arrow">
                    </div>
                        <?php if ( have_rows( 'cadastro_de_variacoes_do_produto' ) ) : ?>
                            <?php $count = 1; while ( have_rows( 'cadastro_de_variacoes_do_produto' ) ) : the_row(); ?>
                               
                                <?php $imagem_tabela = get_sub_field( 'imagem_tabela' ); ?>
                                <?php if ( $imagem_tabela ) : ?>
                                    <div class="box <?php echo $count; ?>">
                                        <img id="imageToSwap" src="<?php echo esc_url( $imagem_tabela['url'] ); ?>" alt="<?php echo esc_attr( $imagem_tabela['alt'] ); ?>" />

                                    </div>
                                <?php endif; ?>
                            <?php $count++; endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                </div>
                <div class="product-form">
                    <?php if ( get_field( 'imagem_sobre_o_produto_-_desktop' ) ) : ?>
                        <img src="<?php the_field( 'imagem_sobre_o_produto_-_desktop' ); ?>"   class="hide-mobile" alt="<?php the_field( 'legenda_sobre_a_imagem_do_produto_desktop' ); ?>" title="<?php the_field( 'legenda_sobre_a_imagem_do_produto_desktop' ); ?>"/>
                    <?php endif ?>

                    <?php if ( get_field( 'imagem_sobre_o_produto_mobile' ) ) : ?>
                        <img src="<?php the_field( 'imagem_sobre_o_produto_mobile' ); ?>"   class="hide-desk" alt="<?php the_field( 'legenda_sobre_a_imagem_do_produto_desktop' ); ?>" title="<?php the_field( 'legenda_sobre_a_imagem_do_produto_desktop' ); ?>"/>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 

$terms = get_the_terms($post->ID, 'aplicacoes');
if( $terms): ?>
    <section class="possibilities">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2>Possíveis <br>aplicações</h2>
                </div>
                <div class="col-md-9">
                    <div class="possibilities_carousel">
                    
                        <?php
                                $terms = get_the_terms($post->ID, 'aplicacoes');

                        
                                if( $terms):
                                    $cont = 1;
                                    foreach( $terms as $t ):
                            ?>
                            <div class="card">
                                <?php
                                        $taxonomy_prefix = 'aplicacoes';
                                        $term_id = $t->term_id ;
                                        $term_id_prefixed = $taxonomy_prefix .'_'. $term_id;

                                        ?>
                                <div class="card-header">
                                    <?php if ( get_field( 'imagem_destacada', $term_id_prefixed ) ) : ?>
                                        <img class="img-fluid" src="<?php the_field( 'imagem_destacada', $term_id_prefixed  ); ?>" />
                                    <?php endif ?>
                                </div>
                                <div class="card-content">
                                    <h3> <?php echo $t->name; ?></h3>
                                    <p> <?php echo $t->description; ?></p>
                                </div>
                            </div>
                            <?php
                                        $cont++;
                                    endforeach;
                                endif;
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /.aplication -->
<?php else: ?>
    
<?php endif; ?>



<section class="related">
 <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Produtos <br>relacionados</h2>
            </div>
            <div class="col-md-9">
                <div class="related_carousel">
                    <?php $produtos_relacionados = get_field( 'produtos_relacionados' ); ?>
                    <?php if ( $produtos_relacionados ) : ?>
                        <?php foreach ( $produtos_relacionados as $post ) : ?>
                            <?php setup_postdata ( $post ); ?>
                                <div class="card">
                                    <div class="card-interno">
                                        <div class="card-header">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                                    echo the_post_thumbnail('full');?>
                                            </a>
                                        </div>
                                        <div class="card-content">
                                            <h3><?php the_title(); ?></h3>
                                            <p><?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?></p>
                                            <div class="d-flex justify-content-end">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                    <button class="btn btn_first mt-4 mb-4">Detalhes do produto</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            <?php endforeach; ?>
                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="/produtos">
                        <button class="btn btn_first mt-4">Todos os produtos</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.related-->

<?php get_template_part( 'global/template-part', 'solution' ); ?>
<?php endwhile; ?>


<?php get_footer(); ?>

<script>
$(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>