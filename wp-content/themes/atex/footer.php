<?php //get_template_part( 'global/template-part', 'newsletter' ); 
?>
<!-- Footer -->
<?php get_template_part('global/template-part', 'comercial-contact'); ?>
<footer class="footer">
    <div class="container-fluid h-100 d-none d-md-block">
        <div class="row h-100 align-items-start justify-content-between">
            <div class="col-md-2">
                <img src='/wp-content/themes/atex/img/logof.png' class='img-fluid' alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' loading='lazy'>
            </div>
            <div class="col-md-2 row m-0 p-0">
                <ul class="footer-menu first-menu">
                                                                                                                                <li>
                                    <a href="<?php echo home_url(); ?>" target=""><b>Home</b></a>
                                </li>
                                                                                                                                            <li>
                                    <a href="<?php echo home_url(); ?>/quem-somos/" target="">Quem somos</a>
                                </li>
                                                                                                                                            <li>
                                    <a href="<?php echo home_url(); ?>/obras/" target="">Obras</a>
                                </li>
                                                                                                                                          
                                                                                                                                            <li>
                                    <a href="<?php echo home_url(); ?>/blog/" target="">Blog</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-2 row m-0 p-0">
                                <ul class="footer-menu">
                                <li class="pb-2">
                                    <a href="#" style="pointer-events: none;" target=""><b>Soluções modulares</b></a>
                                </li>
                                <?php
                    // Custom WP query query
                    // Query Arguments
                    $args_query = array(
                        'post_status' => array('publish'),
                        'posts_per_page' => -1,
                        'post_type' => 'solucoes',
                        'order' => 'DESC',
                    );

                    // The Query
                    $query = new WP_Query($args_query);

                    // The Loop
                    if ($query->have_posts()) {
                        $cont = 0;
                        while ($query->have_posts()) {
                            $query->the_post();
                            // Your custom code 
                    ?>
                            <li class="card_content menu-<?php echo $cont; ?>">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <?php the_title(''); ?>
                                   
                                </a>
                            </li>


                    <?php $cont++;                            }
                    } else {
                        // no posts found

                    }

                    /* Restore original Post Data */
                    wp_reset_postdata();

                    ?>
                            </ul>
                        </div>
                        
                        <div class="col-md-2 row m-0 p-0">
                            <ul class="footer-menu">
                                <li class="pb-2">
                                    <a href="/produtos" target=""><b>Produtos</b></a>
                                </li>
                                <?php
                                // Custom WP query query
                                // Query Arguments
                                $args_query = array(
                                    'post_status' => array('publish'),
                                    'posts_per_page' => -1,
                                    'post_type' => 'pprodutos',
                                    'order' => 'DESC',
                                );

                                // The Query
                                $query = new WP_Query($args_query);
                                $cont = 1;
                                // The Loop
                                if ($query->have_posts()) {
                                    while ($query->have_posts()) {
                                        $query->the_post();
                                        // Your custom code 
                                ?>
                                        <li class="card_content">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"" id="menu-<?php echo $cont ?>">
                                                <?php the_title(''); ?>
                                            
                                            </a>
                                        </li>
                                <?php  $cont++;                         }
                                } else {
                                    // no posts found

                                }

                                /* Restore original Post Data */
                                wp_reset_postdata();

                                ?>
                            </ul>
                        </div>
                        <div class="col-md-2 row m-0 p-0">
                                <ul class="footer-menu">
                                                     <li class="pb-2">
                                                     <a href="#" style="pointer-events: none;" target=""><b>Contato</b></a>
                                </li>
                                                                                                                                            <li>
                                    <a href="<?php echo home_url(); ?>/trabalhe-conosco/" target="">Trabalhe Conosco</a>
                                </li>
                                                                                                                                            <li>
                                    <a href="<?php echo home_url(); ?>/fale-conosco/" target="">Fale Conosco</a>
                                </li>
                                                                                        </ul>
                        </div>
            <div class="col-md-2">
                <div class="col-md-12 px-4 pt-4">
                    <p>Redes Sociais</p>
                    <ul class="list-inline social-media">
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/instagran.svg" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/facebook.svg" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/youtube.svg" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/linkedin.svg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="contato py-0 pl-4 pr-0">
                    <div class="col-md-12 px-0 pt-3 pb-3">
                    <p class="pb-3">Whatsapp</p>
                        <?php
                        $phone = get_field('whatsapp', 'option');
                        $phone = preg_replace('/\D+/', '', $phone);
                        $message = 'Olá, ';
                        ?>
                        <a class="row aligh-items-center m-0" href="https://wa.me/<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank">
                            <img src="/wp-content/themes/atex/img/svg/whatsapp.svg" alt=""> <?php echo $phone_tel; ?>
                            <p class="d-md-block pl-3 contact_number"><?php echo $phone; ?> </p>
                        </a>
                    </div>
                    <div class="col-md-12 px-0 pt-3">
                        <p class="pb-3">Telefone</p>
                        <?php
                        $phone_tel = get_field('telefone', 'option');
                        $phone_tel = preg_replace('/\D+/', '', $phone_tel);
                        ?>
                        <a class="row aligh-items-center m-0" href="tel:+<?php echo $phone_tel; ?>" rel="external" target="_blank">
                            <img src="/wp-content/themes/atex/img/svg/tel.svg" alt="">
                            <p class="d-md-block pl-3 contact_number"><?php echo $phone_tel; ?></p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container h-100 d-block d-md-none">
        <div class="row h-100 align-items-stretch justify-content-between">
            <div class="col-md-2">
                <img src='https://img1.wsimg.com/isteam/ip/de50eed6-c288-4793-8414-9225c0b11302/27376bdc-1685-474b-89a1-824942ebce93.png/:/cr=t:0%25,l:0%25,w:100%25,h:100%25/rs=w:124,cg:true' class='img-fluid' alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' loading='lazy'>
            </div>
            <div class="col-md-8">
                <ul class="footer-menu">
                    <?php if (have_rows('cadastro_de_paginas', 'option')) : ?>
                        <?php while (have_rows('cadastro_de_paginas', 'option')) : the_row(); ?>
                            <?php $pagina = get_sub_field('pagina'); ?>
                            <?php if ($pagina) : ?>
                                <li>
                                    <a href="<?php echo esc_url($pagina['url']); ?>" target="<?php echo esc_attr($pagina['target']); ?>"><?php echo esc_html($pagina['title']); ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found 
                        ?>
                    <?php endif; ?>

                </ul>
            </div>
            <div class="col-md-2">
                <div class="col-md-12 p-4 border-top-footer">
                    <p>Redes Sociais</p>
                    <ul class="list-inline social-media">
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/instagran.svg" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/facebook.svg" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/youtube.svg" alt="">
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" rel="external">
                                <img src="/wp-content/themes/atex/img/svg/linkedin.svg" alt="">
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="contato border-top-footer py-4 pl-4 pr-0">
                    <div class="col-md-12 px-0 pt-3 pb-3">

                        <?php
                        $phone = get_field('whatsapp', 'option');
                        $phone = preg_replace('/\D+/', '', $phone);
                        $message = 'Olá, ';
                        ?>
                        <a class="row aligh-items-center m-0" href="https://wa.me/<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank">
                            <img src="/wp-content/themes/atex/img/svg/whatsapp.svg" alt="">
                            <p class="d-md-block pl-3 contact_number"><?php echo $phone; ?> </p>
                        </a>
                    </div>
                    <div class="col-md-12 px-0 pt-3">

                        <?php
                        $phone_tel = get_field('telefone', 'option');
                        $phone_tel = preg_replace('/\D+/', '', $phone_tel);
                        ?>
                        <a class="row aligh-items-center m-0" href="tel:+<?php echo $phone_tel; ?>" rel="external" target="_blank">
                            <img src="/wp-content/themes/atex/img/svg/tel.svg" alt="">
                            <p class="d-md-block pl-3 contact_number"><?php echo $phone_tel; ?></p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>