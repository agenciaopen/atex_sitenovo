<?php get_header(); ?>
   
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'global/template-part', 'banner' ); ?>
                    <section class="content">
                        <div class="container h-100">
                            <div class="row h-100 align-items-center justify-content-center">
                                <div class="col-md-12">
                                    <h1><?php the_title(); ?></h1>
                                    <p><?php the_content(); ?></p>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-md-12 text-center text-md-right">
                                    <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid' alt='<?php bloginfo( 'name' ); ?>' title='<?php bloginfo( 'name' ); ?>' loading='lazy'>
                                </div>  
                            </div>
                        </div>
                    </section>
                   
                </div>
            <?php endwhile; else: ?>
                <div class="artigo">
                    <h2>Nada Encontrado</h2>
                    <p>Erro 404</p>
                    <p>Lamentamos mas não foram encontrados artigos.</p>
                </div>            
            <?php endif; ?>
   
<?php get_footer(); ?>