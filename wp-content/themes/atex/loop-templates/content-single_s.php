<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php         $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>

<style>
#hero{
     background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(0,0,0,0.4)), to(rgba(0,0,0,0.4))),url(<?php echo $featured_img_url;?>);
	background-image: linear-gradient(rgba(0,0,0,0.4), rgba(0,0,0,0.4)),url(<?php echo $featured_img_url;?>);
	background-attachment: fixed;
	height: 100vh;
}
</style>
<section id="hero" class="">
    <div class="container h-100">
        <div class="row h-100 align-items-end justify-content-center">
            <div class="col-md-8 text-center">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<p class="text-white mt-3"><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);?></p>


            </div>
        </div>
    </div>
</section>
<section id="post_content">
	<div class="container h-100">
		<div class="row h-100 justify-content-center">
 
			<div class="col-md-5 text-left">
				<div class="entry-meta">
					<?php understrap_posted_on(); ?>
				</div><!-- .entry-meta -->
			</div>
			<div class="col-md-5 text-center text-md-right">
				<?php if( has_active_subscription() ){ // Current user has an active subscription 
			}
			else {?>
			<a href="/pagamento">
				<div class="btn btn-primary">
					Assinar
				</div>
			</a>
			<?php } ?>
			</div>
			
	
			<div class="entry-content col-md-10">
			 <?php
      $active_sub = get_post_meta(get_the_ID(), '_wc_memberships_force_public', true);
 ?>
				<?php if(( get_field('liberar_conteudo') == 'sim' || has_active_subscription() || ( 'yes' == get_post_meta(get_the_ID(), '_wc_memberships_force_public', true)))){ // Current user has an active subscription ?>
									<p class=""><?php the_content(); ?></p>
<?php
			}
			else {?>
				<p class="inicial"><?php the_content(); ?></p>
				<?php } ?>
			</div>
		</div>
	</div>
</section>

	 <?php
 ?>
				


	<section class="related">
         <div class="container">
			 <div class="row justify-content-start align-items-start mb-5">
				 		<?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>
			 </div>
             <div class="row justify-content-start align-items-start" id="list_posts">
        
                <?php $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 6, 'post__not_in' => array($post->ID) ) );
					if( $related ){ ?>
						<div class="col-md-12 text-center text-xl-left mb-5">
                    		<h2>Veja também</h2>
                		</div>
						<?php foreach( $related as $post ) {
                        setup_postdata($post); ?>
                        <div class="col-xl-4 col-md-4 article-wrapper mb-5 post_item">
                            <a href="<?php echo get_permalink() ?>">
                                <?php if( get_field('featured_webp', $post->ID) ): ?>
                                    <?php if (webpSupported()) : ?>
                                        <?php $img = get_field('featured_webp', $post->ID); ?>
                                    <?php else:?>
                                        <?php $img = wp_get_attachment_url(get_post_thumbnail_id($post->ID), '' ); ?>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php $img = wp_get_attachment_url(get_post_thumbnail_id($post->ID), '' ); ?>
                                <?php endif; ?>
                                <figure class="tint col-md-12 p-0" style="background-image: url('<?php echo $img; ?>')">
                                </figure><!--/.figure-->
                            
                                <div class="col-md-12 p-0">
                                    <?php 
                                        global $postcat;
                                        $postcat = get_the_category( $post->ID );
                                        $category = get_the_category();
                                        $firstCategory = $category[0]->cat_name;
                                        // Get the ID of a given category
                                        $category_id = get_cat_ID( $firstCategory );
                                        // Get the URL of this category
                                        $category_link = get_category_link( $category_id );
                                    ?>
                                    <p class="col-md-12 p-0"><a href="<?php echo esc_url( $category_link ); ?>"><?php echo $firstCategory; ?></a><p>
                                                                            <a href="<?php echo get_permalink() ?>">
<h2><?php the_title()?></h2></a>
                                                                           <a href="<?php echo get_permalink() ?>">
 <p class=""><?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);?></p></a>
                                </div><!--/.card-body-->
                            </a><!--/.permalink-->
                        </div><!--/.<?php sanitize_title(get_the_title()); ?>-->             
					<?php }}
                wp_reset_postdata(); ?>
                <div class="col-md-12 text-center main">
					<a href="/artigos">
						<div class="btn btn_call bounce-to-right">
							Ver todos
						</div>					
					</a>
				</div>
             </div>
        </div>
    </section>
		
				<?php if(( get_field('liberar_conteudo') == 'sim' || has_active_subscription() || ( 'yes' == get_post_meta(get_the_ID(), '_wc_memberships_force_public', true)))){ // Current user has an active subscription 
    // do something … here goes your code

    // Example of displaying something
	// echo '<p>I have active subscription</p>'; ?>
<?php 
}
else {
?>
<style>
	.entry-content p.inicial:first-child:after {
		position: absolute;
		left: 0;
		height: 150px;
		width: 100%;
		content: "";
		pointer-events: none;
		z-index: 10;
		background: -webkit-gradient(linear, left bottom, left top, from(#fff), to(rgba(255,255,255,0)));
		background: linear-gradient(to top, #fff 0%, rgba(255,255,255,0) 100%);
		top: -20px;
	} 
	.woocommerce p:first-child:after{
		all: unset;
	} 
</style>

<?php get_template_part( 'templates/global/template-part', 'signature' ); ?>


<?php } ?>


</article><!-- #post-## -->
