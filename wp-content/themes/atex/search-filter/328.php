<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
	<div class="results-unidade">
		<ul class="contact-list">
			<?php
				while ($query->have_posts())
				{
					$query->the_post();	
					
					?>
					<div class="result-header col-lg-4">
						<i class="marker">
						<svg xmlns="http://www.w3.org/2000/svg" width="23.92" height="31.322" viewBox="0 0 23.92 31.322">
							<path id="Caminho_178" data-name="Caminho 178" d="M24.138,31.32h-.783l-.153-.063a2.48,2.48,0,0,1-1.09-.94l-2.944-4.156Q16.5,22.4,13.842,18.644a11.319,11.319,0,0,1-1.992-5.491A11.777,11.777,0,0,1,18.816,1.111,11.621,11.621,0,0,1,31.467,2.887a11.338,11.338,0,0,1,4.144,7.664,11.73,11.73,0,0,1-2.192,8.416q-4,5.638-7.99,11.276a2.5,2.5,0,0,1-1.156,1.018Zm-.385-1.7h0a2.168,2.168,0,0,0,.285-.354q3.993-5.638,7.99-11.276a10.074,10.074,0,0,0,1.879-7.226,9.688,9.688,0,0,0-3.517-6.556A9.89,9.89,0,0,0,19.5,2.674a10.08,10.08,0,0,0-5.951,10.317,9.633,9.633,0,0,0,1.682,4.661q2.644,3.759,5.325,7.517,1.479,2.08,2.95,4.166A1.565,1.565,0,0,0,23.752,29.616Zm-.031-13.089h-.044a4.6,4.6,0,1,1,.044,0Zm.031-7.392A2.843,2.843,0,1,0,26.584,12v-.013h0a2.866,2.866,0,0,0-2.8-2.85h-.031Z" transform="translate(-11.782 0.002)" fill="#ffffff"/>
						</svg></i>
						<p><?php the_field( 'endereco' ); ?></p>
					</div>

					<div class="result-content col-lg-5">
						<?php if ( have_rows( 'informacoes_sobre_entregas_e_devolucoes:' ) ) : ?>
							<p><br><b>Informações sobre entregas e devoluções:</b></p>
								<?php while ( have_rows( 'informacoes_sobre_entregas_e_devolucoes:' ) ) : the_row(); ?>
									<?php 
										$phone = get_sub_field( 'telefone', 'option' );
										$phone = preg_replace('/\D+/', '', $phone);
									?>
									<?php the_sub_field( 'extra' ); ?>
									<p>
										<a href="tel:+<?php echo $phone; ?>" rel="external" target="_blank">
											<?php the_sub_field( 'telefone'); ?>
										</a>
									</p>
								<?php endwhile; ?>
						<?php else : ?>
							<?php // no rows found ?>
						<?php endif; ?>	
					</div>	
				<?php
			}
		?>
		</ul>
	</div>
<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>