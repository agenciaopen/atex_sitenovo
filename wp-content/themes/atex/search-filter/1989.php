<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	
	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->
	
				<?php
					while ($query->have_posts())
					{
					$query->the_post();
				
					?>
        <div class="row mb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header col-md-3">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                echo the_post_thumbnail('full');?>
                        </a>
                    </div>
                    <div class="card-description col-md-3">
                        <h3>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                        <p>
                            <?php echo wp_trim_words( get_the_content(), 25, '...' ); ?>
                           
                        </p>
                    </div>
                    <div class="card-soluctions col-md-3">
                        <h3>Usados nas soluções</h3>
                        <ul>
                            <?php
                                $terms = get_terms( 
                                array(
                                'taxonomy' => 'solucoes',
                                
                                ) 
                                );
                                if( $terms):
                                $cont = 1;
                                foreach( $terms as $t ):
                                ?>                
                            <li><img src="./wp-content/themes/atex/img/svg/checked.svg" alt="" class="pr-2"><?php echo $t->name; ?></li>
                            <?php
                                $cont++;
                                endforeach;
                                endif;
                                ?>
                        </ul>
                    </div>
                    <div class="card-related col-md-3">
                        <h3>Produtos Relacionados</h3>
                        <ul>
                            <?php $produtos_relacionados = get_field( 'produtos_relacionados', $post->ID ); ?>
                            <?php if ( $produtos_relacionados ) : 
                                ?>
                                <?php foreach ( $produtos_relacionados as $posta ) : ?>
                                    <?php setup_postdata ( $posta ); ?>
                                        <li>
                                            <img src="./wp-content/themes/atex/img/svg/checked.svg" alt="" class="pr-2">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                    <?php echo $posta->post_title; ?>
                                                </a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
												
				<?php } ?>

<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>

<script>
	
	var title = $("select[name='_sfm_titulo_empreendimento[]'] option:not(.sf-item-0):selected" ).text();
	console.log(title);
	if (title ==''){

	}else{
		$('#title').html('<h2>Empresas parceiros no projeto do <br>' + title + '</h2> <p>Aqui estão listadas todas as empresas que participaram do projeto do <b>' + title + '</b>, desde seu planejamento até sua construção e preservação. Somos gratos a todos os envolvidos nessa parceria. </p>');
	}
</script>