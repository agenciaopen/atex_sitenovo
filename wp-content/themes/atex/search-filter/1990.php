<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $query->have_posts() )
{
	?>
	<h2>Exibindo resultados da sua busca.</h2>
		<div class="row resultadoblog m-0 w-100">
			



	<!-- Found <?php // echo $query->found_posts; ?> Results<br /> -->

				<?php
					while ($query->have_posts())
					{
					$query->the_post();
				
					?>
					<div class="col-md-4 ">
						<div class="card  col-md-12 p-0">

                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <div class="card-header">
                                        <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large'); ?>
                                        <img src="<?php echo $url ?>" class="img-fluid" />
                                    </div>
                                    <div class="card-content">
                                        <h4 class="date"><b><?php the_date(); ?></b></h4>
                                        <h4><?php $categories = get_the_category();
                                            foreach ($categories as $category) {
                                                //echo $category->name; //category name
                                                $cat_link = get_category_link($category->cat_ID);
                                                echo '<strong><a href="' . $cat_link . '">' . $category->name . ' </a></strong> '; // category link
                                            } ?></h4><br>
                                        <h3><?php the_title(); ?></h3><br>
                                        <p><?php 
                                        $excerpt = get_the_excerpt();
 
                                        $excerpt = substr($excerpt, 0, 240);
                                        $result = substr($excerpt, 0, strrpos($excerpt, ' '));
                                        echo $result;
                                        ?>...</p>
                                        <p class="d-none"><?php echo wp_strip_all_tags(get_the_excerpt(), true); ?></p>
                                    </div>
                                </a>
                            </div>
							</div>
		
				<?php } ?>
				</div>
	</section>
<?php
}
else
{
	?>
	<div class='search-filter-results-list text-center mt-4 d-none' data-search-filter-action='infinite-scroll-end'>
		<span>Final dos resultados</span>
	</div>
	<?php
}
?>

<script>
	
	var title = $("select[name='_sfm_titulo_empreendimento[]'] option:not(.sf-item-0):selected" ).text();
	console.log(title);
	if (title ==''){

	}else{
		$('#title').html('<h2>Empresas parceiros no projeto do <br>' + title + '</h2> <p>Aqui estão listadas todas as empresas que participaram do projeto do <b>' + title + '</b>, desde seu planejamento até sua construção e preservação. Somos gratos a todos os envolvidos nessa parceria. </p>');
	}
</script>