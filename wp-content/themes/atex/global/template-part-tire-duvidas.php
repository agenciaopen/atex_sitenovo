<section class="tire-duvidas">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Tire suas duvidas</h2><br>
                <p>Ficou com alguma dúvida sobre os produtos Atex?</p><br>
                <p>Entre em contato e fale com nossos especialistas.</p>
            </div>
            <div class="col-md-8 doubts">
            <script id="bx24_form_inline" data-skip-moving="true"> 
                (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function(){arguments[0].ref=u; 
                        (w[b].forms=w[b].forms||[]).push(arguments[0])}; 
                        if(w[b]['forms']) return; 
                        var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date()); 
                        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h); 
                })(window,document,'https://atex.bitrix24.com.br/bitrix/js/crm/form_loader.js','b24form'); 
        
                b24form({"id":"21","lang":"br","sec":"56b96l","type":"inline"}); 
            </script> 
                
            </div>
        </div>
    </div>
</section><!--/.tire-duvidas-->