<?php
global $post;
//$page_ID = $post->ID;
// get page ID
$page_ID = get_option('page_on_front');

?>
<section class="blog">
    <div class="container">
        <div class="row">
            <div class="col-md-9 order-2 order-md-1 p-0">
                <div class="blog_carousel">
                    <?php
                    // Custom WP query query
                    // Query Arguments
                    $args_query = array(
                        'post_status' => array('publish'),
                        'posts_per_page' => 9,
                        'order' => 'DESC',
                    );

                    // The Query
                    $query = new WP_Query($args_query);

                    // The Loop
                    if ($query->have_posts()) {
                        while ($query->have_posts()) {
                            $query->the_post();
                            // Your custom code 
                    ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <div class="card">
                                    <div class="card-header">
                                        <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large'); ?>
                                        <img src="<?php echo $url ?>" class="img-fluid" />
                                    </div>
                                
                                <div class="card-content">
                                    
                                        <h4 class="date"><b><?php the_date(); ?></b></h4>
                                        <h4><?php $categories = get_the_category();
                                            foreach ($categories as $category) {
                                                //echo $category->name; //category name
                                                $cat_link = get_category_link($category->cat_ID);
                                                echo '<strong>' . $category->name . ' </strong> '; // category link
                                            } ?></h4><br>
                                        <h3><?php the_title(); ?></h3><br>
                                        <p><?php 
                                        $excerpt = get_the_excerpt();
 
                                        $excerpt = substr($excerpt, 0, 180);
                                        $result = substr($excerpt, 0, strrpos($excerpt, ' '));
                                        echo $result;
                                        ?>...</p> 
                                        <p class="d-none"><?php echo wp_strip_all_tags(get_the_excerpt(), true); ?></p>
                                    </div>
                                </div>
                            </a>


                    <?php                            }
                    } else {
                        // no posts found

                    }

                    /* Restore original Post Data */
                    wp_reset_postdata();

                    ?>

                </div>
            </div>
            <div class="col-md-3 card-right order-1 order-md-2">
                <?php $page_ID = 2; ?>
                <h2><?php the_field('titulo_blog', $page_ID); ?></h2>
            </div>
            <div class="col-md-12 order-3 d-flex justify-content-center">
                <a href="/blog/" target="">
                    <button class="btn btn_first mt-4 mb-4">Acessar o blog da Atex</button>
                </a>

            </div>
        </div>
    </div>
</section>