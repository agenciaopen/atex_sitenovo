<?php

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php if ( have_posts() ) : ?>
	
	<?php
if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('/wp-content/uploads/2020/09/banner-home.png');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
                                <h4 class="text-white">
                                    <?php global $wp_query; ?>
									(<?php //echo $wp_query->found_posts;?>)
                                    <?php//printf( esc_html__( 'Veja os resultados da busca abaixo: %s' ), '<span>' . get_field_string() . '</span>'); ?>
                                    <?php 
                                        //replace `140` with the ID of your search form
                                        global $searchandfilter;
                                        $sf_current_query = $searchandfilter->get(140)->current_query();
                                        echo $sf_current_query->get_field_string($searchandfilter);
                                    ?>
                                </h4>
                            </div>
							<div class="col-md-8">
                            <?php echo do_shortcode('[searchandfilter id="1990"]');?>
							<form id="searchform" method="get" action="<?php echo home_url('/'); ?>" class="w-100 d-none">
    <div class="input-group mb-3">
        <input type="text" class="search-field form-control" name="s" placeholder="Procurar" value="<?php the_search_query(); ?>" required="required">
        <input type="hidden" name="post_type[]" value="post" />
        <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">
                <button type="submit" value="Procurar"><i class="fa fa-search"></i></button>
            </span>
        </div>
    </div>
</form>
</div>
                        </div>
                    </div>
                </section><!-- /.main -->

                <section class="results">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center">
                            <?php echo do_shortcode('[searchandfilter id="1990" show="results"]');?>

                        </div>
                    </div>
                </section>
							
					
<?php endif; ?>


<?php get_footer(); ?>