<?php
/**
*
* Template Name: Apoio 
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php get_template_part( 'global/template-part', 'banner' ); ?>

<!--<section id="content">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center h-100">
            
        </div>
    </div>
</section>/.content-->

<section class="files">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2><?php the_field( 'titulo_materiais_disponiveis' ); ?></h2>
                <p><?php the_field( 'descricao_materiais_disponiveis' ); ?></p>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-6 pl-0">
                <?php if ( have_rows( 'cadastro_de_materiais' ) ) : ?>
                    <ul class="list-files">
                        <?php while ( have_rows( 'cadastro_de_materiais' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'conteudo_privado' ) == 0 ) : ?>
                                <li class="pb-3">
                                    <?php $arquivo = get_sub_field( 'arquivo' ); 
                                    ?>
                                    <?php if ( $arquivo ) : ?>
                                        <div class="archive open">
                                            <img class="pr-2" src="/wp-content/themes/atex/img/svg/open.svg" alt="">
                                            <a href="<?php echo esc_url( $arquivo['url'] ); ?>" target="_blank">
                                            <?php echo esc_html( $arquivo['title'] ); ?></a>
                                        </div>
                                        <div class="padlock">
                                            <img src="/wp-content/themes/atex/img/svg/unlock.svg" alt="">
                                        </div>
                                    <?php endif; ?>
                                </li>
                                <?php // echo 'false'; ?>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </ul>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
            <div class="col-md-6 pl-0">
                <?php if ( have_rows( 'cadastro_de_materiais' ) ) : ?>
                    <ul class="list-files">
                        <?php while ( have_rows( 'cadastro_de_materiais' ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'conteudo_privado' ) == 1) : ?>
                                <?php 
                                    if ( is_user_logged_in() ) {
                                        $access = 'open'; 
                                        $img_access = '/wp-content/themes/atex/img/svg/open.svg';
                                        $img_access_icon = '/wp-content/themes/atex/img/svg/unlock.svg';

                                    } else {
                                        // your code for logged out user 
                                        $access = 'closed';
                                        $img_access = '/wp-content/themes/atex/img/svg/close.svg';
                                        $img_access_icon = '/wp-content/themes/atex/img/svg/lock.svg'; 

                                    }

                                    ?>
                                    <li class="pb-3">
                                        <?php $arquivo = get_sub_field( 'arquivo' ); 
                                             ?>
                                        <?php if ( $arquivo ) : ?>
                                            <div class="archive <?php echo $access;?>">
                                                <img class="pr-2" src="<?php echo $img_access;?>" alt=""> 
                                                <?php 
                                                                                    if ( is_user_logged_in() ) {
?>
                                                <a href="<?php echo esc_url( $arquivo['url'] ); ?>" target="_blank">
                                                <?php echo esc_html( $arquivo['title'] ); ?></a>
                                                <?php     } else { ?>
                                                    <?php echo esc_html( $arquivo['title'] ); ?>
                                                    <?php } ?>
                                            </div>
                                            <div class="padlock">
                                                <img src="<?php echo $img_access_icon;?>" alt="">
                                            </div>
                                        </li> 
                                    <?php endif; ?>
                                <?php // echo 'true'; ?>
                            <?php else : ?>
                                <?php // echo 'false'; ?>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    </ul>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!--/.files-->

<section class="account">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                    <h2><?php the_field( 'titulo_acesso' ); ?></h2>
                    <p><?php the_field( 'descricao_acesso' ); ?></p>
                </div>
            <div class="col-md-6">
            </div>
            <div class="account-login col-md-5">
                <h3>Ja é cadastrado? <br>Faça o login.</h3>
                <?php $args = array(
                    'echo' => true,
                    'redirect' => 'http://atex.open/apoio',
                    'form_id' => 'loginform',
                    'label_username' => __( 'Username' ),
                    'label_password' => __( 'Password' ),
                    'label_remember' => __( 'Remember Me' ),
                    'label_log_in' => __( 'Log In' ),
                    'id_username' => 'user_login',
                    'id_password' => 'user_pass',
                    'id_remember' => 'rememberme',
                    'id_submit' => 'wp-submit',
                    'remember' => true,
                    'value_username' => NULL,
                    'value_remember' => false );
                    wp_login_form($args);
                ?>
            </div>
            <div class="accont-create col-md-7">
                <h3>Ainda não <br> possui cadastro?</h3>
                <?php echo do_shortcode('[contact-form-7 id="1783" title="Registro de usuário"]');?>
            </div>
        </div>
</section><!--/.account-->

<?php get_footer(); ?>