<?php

/**
 *
 * Template Name: Busca Blog
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID 
?>


<?php get_template_part('global/template-part', 'banner-busca'); ?>

<section class="search_article">
    <div class="container">
        <div class="row">
               <?php echo do_shortcode('[searchandfilter id="1990" show="results"]');?>
        </div>
    </div>
</section>

<?php get_template_part('global/template-part', 'newsletter'); ?>

<?php get_template_part('global/template-part', 'aqui-tem-atex'); ?>

<?php get_footer(); ?>