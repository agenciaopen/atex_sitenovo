<div class="col-md-4">

<div class="card">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <div class="card-header">
                                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'large' ); ?>
                                            <img src="<?php echo $url ?>" class="img-fluid"/>
                                            </div>
                                            <div class="card-content">
                                                <h4 class="date"><b><?php the_date(); ?></b></h4>
                                                <h4><?php $categories = get_the_category();
                                                foreach($categories as $category){
                                                    //echo $category->name; //category name
                                                    $cat_link = get_category_link($category->cat_ID);
                                                    echo '<strong><a href="'.$cat_link.'">'.$category->name.' </a></strong> '; // category link
                                                }?></h4><br>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                                <h3><?php the_title(); ?></h3><br>
                                                <p><?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?></p></a>
                                            </div>
                                        </a>
                                    </div>                    
                                        </div>