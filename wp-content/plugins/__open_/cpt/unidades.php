<?php
//  register post type unidades 

function open_register_post_type_unidades(){

    $labels = array(
        'name'                  => _x( 'Unidades', 'Post type general name', 'open' ),
        'singular_name'         => _x( 'Unidade', 'Post type singular name', 'open' ),
        'menu_name'             => _x( 'Unidades', 'Admin Menu text', 'open' ),
        'name_admin_bar'        => _x( 'Unidade', 'Adicionar nova on Toolbar', 'open' ),
        'add_new'               => __( 'Adicionar nova', 'open' ),
        'add_new_item'          => __( 'Adicionar nova unidade', 'open' ),
        'new_item'              => __( 'Nova unidade', 'open' ),
        'edit_item'             => __( 'Editar unidade', 'open' ),
        'view_item'             => __( 'Ver unidade', 'open' ),
        'all_items'             => __( 'Todas as unidades', 'open' ),
        'search_items'          => __( 'Procurar unidades', 'open' ),
        'parent_item_colon'     => __( 'Parent unidades:', 'open' ),
        'not_found'             => __( 'Nenhuma unidade foi encontrada', 'open' ),
        'not_found_in_trash'    => __( 'Nenhuma unidade na lixeira', 'open' ),
        'featured_image'        => _x( 'Imagem destacada da unidade', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'open' ),
        'set_featured_image'    => _x( 'Selecionar imagem destacada', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'open' ),
        'remove_featured_image' => _x( 'Remover imagem destacada', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'open' ),
        'use_featured_image'    => _x( 'Usar como imagem destacada', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'open' ),
        'archives'              => _x( 'Arquivos de unidade', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'open' ),
        'insert_into_item'      => _x( 'Inserir na unidade', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'open' ),
        'uploaded_to_this_item' => _x( 'Upload na unidade', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'open' ),
        'filter_items_list'     => _x( 'Filtrar lista de unidades', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'open' ),
        'items_list_navigation' => _x( 'unidades list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'open' ),
        'items_list'            => _x( 'unidades list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'open' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'unidades' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_icon'          => 'dashicons-location-alt',
        'rewrite' => array(
          'slug' => 'unidades',
          'with_front' => false
      ),
        'menu_position'      => null,
        'supports'           => array( 'title', 'author', 'revisions' ),
    );
 
    register_post_type( 'unidades', $args );

}
add_action ('init', 'open_register_post_type_unidades');


  // Let us create Taxonomy for Custom Post Type
  add_action( 'init', 'create_emp_custom_taxonomy_pais', 0 );
     
  //create a custom taxonomy name it "type" for your posts
  function create_emp_custom_taxonomy_pais() {
   
    $labels = array(
      'name' => _x( 'País', 'taxonomy general name' ),
      'singular_name' => _x( 'País', 'taxonomy singular name' ),
      'search_items' =>  __( 'Procurar por tipos' ),
      'all_items' => __( 'Todos os paises' ),
      'parent_item' => __( 'Parent tipo' ),
      'parent_item_colon' => __( 'Parent Type:' ),
      'edit_item' => __( 'Editar tipo' ), 
      'update_item' => __( 'Atualizar tipo' ),
      'add_new_item' => __( 'Adicionar novo País' ),
      'new_item_name' => __( 'Novo País' ),
      'menu_name' => __( 'Países' ),
    ); 	
   
    register_taxonomy('pais',array('unidades'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'pais' ),
    ));
  }

  // Let us create Taxonomy for Custom Post Type
  add_action( 'init', 'create_emp_custom_taxonomy', 0 );
     
  //create a custom taxonomy name it "type" for your posts
  function create_emp_custom_taxonomy() {
   
    $labels = array(
      'name' => _x( 'Estado', 'taxonomy general name' ),
      'singular_name' => _x( 'Estado', 'taxonomy singular name' ),
      'search_items' =>  __( 'Procurar por tipos' ),
      'all_items' => __( 'Todos os tipos' ),
      'parent_item' => __( 'Parent tipo' ),
      'parent_item_colon' => __( 'Parent Type:' ),
      'edit_item' => __( 'Editar tipo' ), 
      'update_item' => __( 'Atualizar tipo' ),
      'add_new_item' => __( 'Adicionar novo Estado' ),
      'new_item_name' => __( 'Novo Estado' ),
      'menu_name' => __( 'Estados' ),
    ); 	
   
    register_taxonomy('estado',array('unidades'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
      'rewrite' => array( 'slug' => 'estado' ),
    ));
  }
  
  