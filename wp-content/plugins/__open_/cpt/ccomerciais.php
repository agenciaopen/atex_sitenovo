<?php
//  register post type ccomerciais 

function open_register_post_type_ccomerciais(){

    $labels = array(
        'name'                  => _x( 'Contatos Comerciais', 'Post type general name', 'open' ),
        'singular_name'         => _x( 'Contato comercial', 'Post type singular name', 'open' ),
        'menu_name'             => _x( 'Contatos Comerciais', 'Admin Menu text', 'open' ),
        'name_admin_bar'        => _x( 'Contato comercial', 'Adicionar novo on Toolbar', 'open' ),
        'add_new'               => __( 'Adicionar novo', 'open' ),
        'add_new_item'          => __( 'Adicionar novo Contato comercial', 'open' ),
        'new_item'              => __( 'Nova Contato comercial', 'open' ),
        'edit_item'             => __( 'Editar Contato comercial', 'open' ),
        'view_item'             => __( 'Ver Contato comercial', 'open' ),
        'all_items'             => __( 'Todas os Contatos Comerciais', 'open' ),
        'search_items'          => __( 'Procurar Contatos Comerciais', 'open' ),
        'parent_item_colon'     => __( 'Parent Contatos Comerciais:', 'open' ),
        'not_found'             => __( 'Nenhuma Contato comercial foi encontrada', 'open' ),
        'not_found_in_trash'    => __( 'Nenhuma Contato comercial na lixeira', 'open' ),
        'featured_image'        => _x( 'Imagem destacada da Contato comercial', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'open' ),
        'set_featured_image'    => _x( 'Selecionar imagem destacada', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'open' ),
        'remove_featured_image' => _x( 'Remover imagem destacada', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'open' ),
        'use_featured_image'    => _x( 'Usar como imagem destacada', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'open' ),
        'archives'              => _x( 'Arquivos de Contato comercial', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'open' ),
        'insert_into_item'      => _x( 'Inserir no Contato comercial', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'open' ),
        'uploaded_to_this_item' => _x( 'Upload no Contato comercial', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'open' ),
        'filter_items_list'     => _x( 'Filtrar lista de Contatos Comerciais', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'open' ),
        'items_list_navigation' => _x( 'Contatos Comerciais list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'open' ),
        'items_list'            => _x( 'Contatos Comerciais list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'open' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_icon'          => 'dashicons-groups',
        'menu_position'      => null,
        'rewrite' => array(
            'slug' => 'contatos-comerciais',
            'with_front' => false
        ),
        'supports'           => array( 'title', 'author', 'revisions' ),
    );
 
    register_post_type( 'ccomerciais', $args );

}
add_action ('init', 'open_register_post_type_ccomerciais');

