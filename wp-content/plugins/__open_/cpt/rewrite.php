<?php 
register_activation_hook( __FILE__, 'my_rewrite_flush' );

function my_rewrite_flush() {
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry, 
    // when you add a post of this CPT.
    open_register_post_type_obras();
    open_register_post_type_ccomerciais();
    open_register_post_type_pprodutos();
    open_register_post_type_sistemas();
    open_register_post_type_unidades();
    
    // reference -> https://developer.wordpress.org/reference/functions/register_post_type/#flushing-rewrite-on-activation
    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}