<?php
//  register post type solucoes 

function open_register_post_type_solucoes(){

    $labels = array(
        'name'                  => _x( 'Soluções', 'Post type general name', 'open' ),
        'singular_name'         => _x( 'Solução', 'Post type singular name', 'open' ),
        'menu_name'             => _x( 'Soluções', 'Admin Menu text', 'open' ),
        'name_admin_bar'        => _x( 'Solução', 'Adicionar novo on Toolbar', 'open' ),
        'add_new'               => __( 'Adicionar nova', 'open' ),
        'add_new_item'          => __( 'Adicionar nova Solução', 'open' ),
        'new_item'              => __( 'Nova Solução', 'open' ),
        'edit_item'             => __( 'Editar Solução', 'open' ),
        'view_item'             => __( 'Ver Solução', 'open' ),
        'all_items'             => __( 'Todas os Soluções', 'open' ),
        'search_items'          => __( 'Procurar Soluções', 'open' ),
        'parent_item_colon'     => __( 'Parent Soluções:', 'open' ),
        'not_found'             => __( 'Nenhuma Solução foi encontrada', 'open' ),
        'not_found_in_trash'    => __( 'Nenhuma Solução na lixeira', 'open' ),
        'featured_image'        => _x( 'Imagem destacada da Solução', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'open' ),
        'set_featured_image'    => _x( 'Selecionar imagem destacada', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'open' ),
        'remove_featured_image' => _x( 'Remover imagem destacada', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'open' ),
        'use_featured_image'    => _x( 'Usar como imagem destacada', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'open' ),
        'archives'              => _x( 'Arquivos de Solução', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'open' ),
        'insert_into_item'      => _x( 'Inserir no Solução', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'open' ),
        'uploaded_to_this_item' => _x( 'Upload no Solução', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'open' ),
        'filter_items_list'     => _x( 'Filtrar lista de Soluções', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'open' ),
        'items_list_navigation' => _x( 'Soluções list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'open' ),
        'items_list'            => _x( 'Soluções list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'open' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'rewrite'            => array( 'slug' => 'solucao' ),
        'show_in_menu'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_icon'          => 'dashicons-screenoptions',
        'menu_position'      => null,
        'rewrite' => array(
            'slug' => 'solucao',
            'with_front' => false
        ),
        'supports'           => array( 'title', 'author', 'revisions', 'thumbnail', 'editor' ),
    );
 
    register_post_type( 'solucoes', $args );

}
add_action ('init', 'open_register_post_type_solucoes');

